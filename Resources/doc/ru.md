ShirockovanSonataAdminPropelSortableBundle
============

Компонент для работы с propel-бихейвором [sortable](http://propelorm.org/behaviors/sortable.html) в админке, сгенерированной при помощи [SonataAdminBundle](http://sonata-project.org/bundles/admin/master/doc/index.html).

## Установка

1. Скачать ShirockovanSonataAdminPropelSortableBundle используя композер.
2. Подключить бандл.
3. Подключить бихейвор в модели.
4. Добавляем кастомный экшн в метод configureListFields сгенерированном Admin-классе.
5. Переопределяем в Admin-классе метод configureRoutes.
6. Обновляем конфигурацию Admin-модуля.
7. Добавляем немного коричневой магии в Admin-класс.

### Шаг 1
Скачать ShirockovanImageCropBundle используя композер используя композер. Добавьте ShirockovanImageCropBundle в composer.json:

```js
{
    "require": {
        "shirockovan/sonata-admin-propel-sortable-bundle": "dev-master"
	},
    "repositories": [
        { "type": "git", "url": "http://git.shirockovan.com/bundles/sonata-admin-propel-sortable-bundle.git" }
    ],
}
```

Теперь запустите композер, чтобы скачать бандл командой:

``` bash
$ composer update shirockovan/imagecrop-bundle
```

Композер установит бандл в папку проекта `vendor/shirockovan/sonata-admin-propel-sortable-bundle`.

### Шаг 2
Подключить бандл

```php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Shirockovan\SonataAdminPropelSortableBundle\ShirockovanSonataAdminPropelSortableBundle(),
    );
}
```

### Шаг 3
Подключить бихейвор в модели

```xml
    <table name="article">
        <column name="id" type="integer" required="true" primaryKey="true" autoIncrement="true" />
        <column name="title" type="varchar" required="true" size="255" />
        <column name="description" type="varchar" required="false" size="255" />
        <column name="url" type="varchar" required="false" size="255" />
        <column name="publish" type="boolean" required="false" />
        <behavior name="sortable" />
    </table>	
```

Бихейвор подлючается с настройками по-умолчанию. Поле с порядком должно называться sortable_rank, иначе бихейвор не будет работать!

### Шаг 4
Добавляем кастомный экшн в метод configureListFields сгенерированном Admin-классе.

```php
<?php

$listMapper
	->add('_action', 'actions', array(
                'label'   => 'Действия',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'move' => array('template' => 'ShirockovanSonataAdminPropelSortableBundle:Admin:_sort.html.twig')
                )
            ))
```

### Шаг 5
Переопределяем в Admin-классе метод configureRoutes следующим образом:

```php
<?php

protected function configureRoutes(Sonata\AdminBundle\Route\RouteCollection $collection)
{
	parent::configureRoutes($collection);
	$collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
}
```

### Шаг 6
Обновляем конфигурацию Admin-модуля в файле services.yml. Файл должен выглядеть следующим образом:

```yml
services:
    artsofte_main.admin.team:
        class: Artsofte\MainBundle\Admin\ArticleAdmin
        arguments: [~, Artsofte\MainBundle\Model\Article, ShirockovanSonataAdminPropelSortableBundle:SortableAdmin]
        arguments:
            - ~
            - Artsofte\MainBundle\Model\Article
            - ShirockovanSonataAdminPropelSortableBundle:SortableAdmin
        calls:
            - [ setContainer, [ @service_container ] ]
            - [ setPositionService, [ @shirockovan_sonata_admin_propel_sortable.position ]]
        tags:
            - {name: sonata.admin, manager_type: propel, group: Сайт, label: Статьи}
```

### Шаг 7
Добавляем немного коричневой магии в Admin-класс:

```php
<?php

public $last_position = 0;

private $container;
private $position_service;

public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container)
{
	$this->container = $container;
}

public function setPositionService(\Shirockovan\SonataAdminPropelSortableBundle\Services\PositionHandler $position_handler)
{
	$this->position_service = $position_handler;
	$this->position_service->setModel($this->getRoot()->getClass());
}


protected $datagridValues = array(
	'_sort_order' => 'ASC',
	'_sort_by' => 'sortable_rank',
);
```

Наслаждаемся прекрасной работой бандла и восхваляем автора сего великолепия!
