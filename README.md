ShirockovanSonataAdminPropelSortableBundle
============

Introduction
------------
Component allows you to use Propel sortable behavior at SonataAdmin interfaces.

Documentation
-------------

For more information, see the documentation: [RU](http://git.shirockovan.com/bundles/sonata-admin-propel-sortable-bundle/blob/master/Resources/doc/ru.md).
