<?php
/**
 * Created by JetBrains PhpStorm.
 * User: validol
 * Date: 27.06.14
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */

namespace Shirockovan\SonataAdminPropelSortableBundle\Services;


class PositionHandler
{

    protected $model;
    protected $query_class;
    protected $peer_class;

    public function __construct()
    {

    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setQueryClass($query_class)
    {
        $this->query_class = $query_class;
    }

    public function getQueryClass()
    {
        if (!$this->query_class) {
            $this->query_class = $this->model.'Query';
        }
        return $this->query_class;
    }

    public function setPeerClass($peer_class)
    {
        if (!$this->peer_class) {
            $this->peer_class = $this->mode.'Peer';
        }
        $this->peer_class = $peer_class;
    }

    public function getPeerClass()
    {
        return $this->peer_class;
    }


    public function getPosition( $object, $position, $last_position )
    {
        switch ($position) {
            case 'up' :
                if ($object->getSortableRank() > 0) {
                    $new_position = $object->getSortableRank() - 1;
                }
                break;

            case 'down':
                if ($object->getSortableRank() < $last_position) {
                    $new_position = $object->getSortableRank() + 1;
                }
                break;

            case 'top':
                if ($object->getSortableRank() > 0) {
                    $new_position = 0;
                }
                break;

            case 'bottom':
                if ($object->getSortableRank() < $last_position) {
                    $new_position = $last_position;
                }
                break;

            default: $new_position = 0;
        }

        return $new_position;
    }

    public function getLastPosition()
    {
        $query_class = $this->getQueryClass();
        $last = $query_class::create()->orderBySortableRank(\Criteria::DESC)->findOneOrCreate();
        return $last->getSortableRank() === null ? -1 : $last->getSortableRank();
    }

    public function updateItems( $object,  $rank, $old_rank, $position)
    {
        $query_class = $this->getQueryClass();
        switch ($position) {
            case 'up':
            case 'top':
                $items = $query_class::create()
                    ->filterBySortableRank($rank, \Criteria::GREATER_EQUAL)
                    ->_and()
                    ->filterBySortableRank($old_rank, \Criteria::LESS_EQUAL)
                    ->filterById($object->getId(), \Criteria::NOT_EQUAL)->find();
                foreach ($items as $item) {
                    $item->setSortableRank($item->getSortableRank() + 1);
                    $item->save();
                }
                break;

            case 'down':
            case 'bottom':
                $items = $query_class::create()
                    ->filterBySortableRank($rank, \Criteria::LESS_EQUAL)
                    ->_and()
                    ->filterBySortableRank($old_rank, \Criteria::GREATER_THAN)
                    ->filterById($object->getId(), \Criteria::NOT_EQUAL)
                    ->find();
                foreach ($items as $item) {
                    $item->setSortableRank($item->getSortableRank() - 1);
                    $item->save();
                }
                break;
        }
    }

}
