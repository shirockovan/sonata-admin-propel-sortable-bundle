<?php

namespace Shirockovan\SonataAdminPropelSortableBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class SortableAdminController extends CRUDController
{

    public function moveAction(Request $request, $id, $position)
    {
        $id     = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        $position_service = $this->get('shirockovan_sonata_admin_propel_sortable.position');
        $position_service->setModel( get_class($object) );
        $rank = $position_service->getPosition( $object, $position, $position_service->getLastPosition() );
        $old_rank = $object->getSortableRank();
        $object->setSortableRank($rank);
        $object->save();
        $position_service->updateItems($object, $rank, $old_rank, $position);

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'success' => true,
            ));
        }

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

}
